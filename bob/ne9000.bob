<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>NE9000 main</name>
  <width>540</width>
  <height>620</height>
  <widget type="rectangle" version="2.0.0">
    <name>rectReadbacks</name>
    <x>298</x>
    <y>60</y>
    <width>220</width>
    <height>230</height>
    <line_width>2</line_width>
    <line_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </line_color>
    <background_color>
      <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
      </color>
    </background_color>
    <corner_width>10</corner_width>
    <corner_height>10</corner_height>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>rectSetpoints</name>
    <x>18</x>
    <y>60</y>
    <width>270</width>
    <height>230</height>
    <line_width>2</line_width>
    <line_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </line_color>
    <background_color>
      <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
      </color>
    </background_color>
    <corner_width>10</corner_width>
    <corner_height>10</corner_height>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>title-bar</name>
    <class>TITLE-BAR</class>
    <width>540</width>
    <height>50</height>
    <line_width>0</line_width>
    <background_color>
      <color name="PRIMARY-HEADER-BACKGROUND" red="151" green="188" blue="202">
      </color>
    </background_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>title</name>
    <text>$(P)$(R)</text>
    <x>10</x>
    <width>340</width>
    <height>50</height>
    <font>
      <font family="Liberation Sans" style="BOLD" size="18.0">
      </font>
    </font>
    <vertical_alignment>1</vertical_alignment>
    <wrap_words>false</wrap_words>
  </widget>
  <widget type="label" version="2.0.0">
    <name>subtitle</name>
    <class>SUBTITLE</class>
    <text>New Era Pump System</text>
    <x>280</x>
    <y>16</y>
    <width>250</width>
    <height>30</height>
    <font>
      <font family="Liberation Sans" style="BOLD" size="16.0">
      </font>
    </font>
    <foreground_color>
      <color name="HEADER-TEXT" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>2</vertical_alignment>
    <wrap_words>false</wrap_words>
  </widget>
  <widget type="group" version="2.0.0">
    <name>grpControls</name>
    <x>298</x>
    <y>300</y>
    <width>220</width>
    <height>230</height>
    <style>3</style>
    <background_color>
      <color name="BACKGROUND" red="220" green="225" blue="221">
      </color>
    </background_color>
    <transparent>true</transparent>
    <widget type="rectangle" version="2.0.0">
      <name>rectControls</name>
      <width>220</width>
      <height>230</height>
      <line_width>2</line_width>
      <line_color>
        <color name="GROUP-BORDER" red="150" green="155" blue="151">
        </color>
      </line_color>
      <background_color>
        <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
        </color>
      </background_color>
      <corner_width>10</corner_width>
      <corner_height>10</corner_height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>grpTitleControls</name>
      <text>Controls</text>
      <width>220</width>
      <height>30</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>butStart</name>
      <actions execute_as_one="true">
        <action type="write_pv">
          <pv_name>$(pv_name)</pv_name>
          <value>1</value>
          <description>set run</description>
        </action>
        <action type="write_pv">
          <pv_name>loc://trigger_run_$(DID)(0)</pv_name>
          <value>1</value>
          <description>set trigger</description>
        </action>
      </actions>
      <pv_name>$(P)$(R)RUN</pv_name>
      <text>Start</text>
      <x>42</x>
      <y>38</y>
      <width>131</width>
      <scripts>
        <script file="EmbeddedPy">
          <text><![CDATA[# Embedded python script
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil

logger = ScriptUtil.getLogger()

pvTotalVolume = PVUtil.getDouble(pvs[1])    # loc://total_volume_$(DID)(0)
pvVolume = PVUtil.getDouble(pvs[2])         # $(P)$(R)VOLUME
pvDirection = PVUtil.getInt(pvs[3])         # $(P)$(R)DIRECTION
pvVolumeWdr = PVUtil.getDouble(pvs[4])         # $(P)$(R)VOLUME_WITHDRAWN

#logger.info("pvDirection %d" % pvDirection)

if ((pvDirection == 1) and (pvVolumeWdr >= pvTotalVolume)):
    # only set max tank volume when withdrawing
    pvs[1].setValue(pvTotalVolume + pvVolume)
]]></text>
          <pv_name>loc://trigger_run_$(DID)(0)</pv_name>
          <pv_name trigger="false">loc://total_volume_$(DID)(0)</pv_name>
          <pv_name trigger="false">$(P)$(R)VOLUME</pv_name>
          <pv_name trigger="false">$(P)$(R)DIRECTION</pv_name>
          <pv_name trigger="false">$(P)$(R)VOLUME_WITHDRAWN</pv_name>
        </script>
      </scripts>
      <tooltip>Start pumping</tooltip>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>butStop</name>
      <actions>
        <action type="write_pv">
          <pv_name>$(pv_name)</pv_name>
          <value>1</value>
          <description>Write PV</description>
        </action>
      </actions>
      <pv_name>$(P)$(R)STOP</pv_name>
      <text>Stop</text>
      <x>42</x>
      <y>78</y>
      <width>131</width>
      <tooltip>Stop pumping</tooltip>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblFirmware</name>
      <text>Firmware:</text>
      <x>10</x>
      <y>199</y>
      <width>60</width>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>txtFirmwareVall</name>
      <pv_name>$(P)$(R)FIRMWARE</pv_name>
      <x>75</x>
      <y>199</y>
      <width>130</width>
      <background_color>
        <color red="200" green="205" blue="201">
        </color>
      </background_color>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>butPause</name>
      <actions>
        <action type="write_pv">
          <pv_name>$(pv_name)</pv_name>
          <value>1</value>
          <description>Write PV</description>
        </action>
      </actions>
      <pv_name>$(P)$(R)PAUSE</pv_name>
      <text>Pause</text>
      <x>42</x>
      <y>118</y>
      <width>131</width>
      <tooltip>1 click to pause 2 clicks to stop</tooltip>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>butCalib</name>
      <actions>
        <action type="write_pv">
          <pv_name>$(pv_name)</pv_name>
          <value>1</value>
          <description>Write PV</description>
        </action>
      </actions>
      <pv_name>$(P)$(R)CALIB</pv_name>
      <text>Calibrate</text>
      <x>42</x>
      <y>160</y>
      <width>131</width>
      <tooltip>Calibrates the tubing diameter using informed volume in setpoints</tooltip>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
  </widget>
  <widget type="group" version="2.0.0">
    <name>grpMonitor</name>
    <x>18</x>
    <y>300</y>
    <width>270</width>
    <height>230</height>
    <style>3</style>
    <background_color>
      <color name="BACKGROUND" red="220" green="225" blue="221">
      </color>
    </background_color>
    <transparent>true</transparent>
    <widget type="rectangle" version="2.0.0">
      <name>rectMonitor</name>
      <width>270</width>
      <height>230</height>
      <line_width>2</line_width>
      <line_color>
        <color name="GROUP-BORDER" red="150" green="155" blue="151">
        </color>
      </line_color>
      <background_color>
        <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
        </color>
      </background_color>
      <corner_width>10</corner_width>
      <corner_height>10</corner_height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>grpTitleMonitor</name>
      <text>Monitoring (volume)</text>
      <width>270</width>
      <height>30</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblVolumeWithdrawn</name>
      <text>Withdrawn:</text>
      <x>25</x>
      <y>48</y>
      <width>75</width>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblVolumeInfused</name>
      <text>Dispensed:</text>
      <x>25</x>
      <y>104</y>
      <width>75</width>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>txtVolumeWDRRead</name>
      <pv_name>$(P)$(R)VOLUME_WITHDRAWN</pv_name>
      <x>57</x>
      <y>72</y>
      <width>60</width>
      <format>1</format>
      <precision>3</precision>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>txtVolumeINFRead</name>
      <pv_name>$(P)$(R)VOLUME_INFUSED</pv_name>
      <x>57</x>
      <y>126</y>
      <width>60</width>
      <format>1</format>
      <precision>3</precision>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>txtMonitorWDRUnits</name>
      <pv_name>$(P)$(R)VOLUME_DIS_UNIT</pv_name>
      <x>117</x>
      <y>72</y>
      <width>45</width>
      <format>6</format>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>txtMonitorINFUnits</name>
      <pv_name>$(P)$(R)VOLUME_DIS_UNIT</pv_name>
      <x>117</x>
      <y>126</y>
      <width>45</width>
      <format>6</format>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="tank" version="2.0.0">
      <name>tankVolumes</name>
      <pv_name>loc://tank_level_$(DID)(0)</pv_name>
      <x>187</x>
      <y>30</y>
      <width>55</width>
      <height>190</height>
      <font>
        <font family="Liberation Sans" style="REGULAR" size="10.0">
        </font>
      </font>
      <background_color>
        <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
        </color>
      </background_color>
      <fill_color>
        <color red="26" green="51" blue="153">
        </color>
      </fill_color>
      <empty_color>
        <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
        </color>
      </empty_color>
      <rules>
        <rule name="Maximum" prop_id="maximum" out_exp="true">
          <exp bool_exp="pv0 &gt; 0">
            <expression>pv0</expression>
          </exp>
          <pv_name>loc://total_volume_$(DID)(0)</pv_name>
        </rule>
      </rules>
      <scripts>
        <script file="EmbeddedPy">
          <text><![CDATA[from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil

logger = ScriptUtil.getLogger()

pvVolumeWdr = PVUtil.getDouble(pvs[0])         # $(P)$(R)VOLUME_WITHDRAWN
pvVolumeInf = PVUtil.getDouble(pvs[1])         # $(P)$(R)VOLUME_INFUSED
pvTankMax = PVUtil.getDouble(pvs[2])         # loc://tank_level_$(DID)(0)
pvTotalVolume = PVUtil.getDouble(pvs[3])    # loc://total_volume_$(DID)(0)

# update current level (Withdrawn - Infused)
pvs[2].write(pvVolumeWdr - pvVolumeInf)

# check if maximum tank level is fewer than withdrawn volume so far, then increase it bit by bit...
if (pvTotalVolume < pvVolumeWdr):
    pvs[3].write(pvVolumeWdr)]]></text>
          <pv_name>$(P)$(R)VOLUME_WITHDRAWN</pv_name>
          <pv_name>$(P)$(R)VOLUME_INFUSED</pv_name>
          <pv_name>loc://tank_level_$(DID)(0)</pv_name>
          <pv_name>loc://total_volume_$(DID)(0)</pv_name>
        </script>
      </scripts>
      <limits_from_pv>false</limits_from_pv>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>butClear</name>
      <actions execute_as_one="true">
        <action type="write_pv">
          <pv_name>$(pv_name)</pv_name>
          <value>1</value>
          <description>set clear</description>
        </action>
        <action type="write_pv">
          <pv_name>loc://total_volume_$(DID)(0)</pv_name>
          <value>0</value>
          <description>reset total_volume</description>
        </action>
      </actions>
      <pv_name>$(P)$(R)CLEAR_V_DISPENSED</pv_name>
      <text>Clear volumes</text>
      <x>36</x>
      <y>174</y>
      <width>120</width>
      <tooltip>Set monitored volumes to "0.00"</tooltip>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
  </widget>
  <widget type="group" version="2.0.0">
    <name>grpSetpoints</name>
    <x>18</x>
    <y>60</y>
    <width>270</width>
    <height>230</height>
    <style>3</style>
    <background_color>
      <color name="BACKGROUND" red="220" green="225" blue="221">
      </color>
    </background_color>
    <transparent>true</transparent>
    <widget type="label" version="2.0.0">
      <name>grpTitleSetpoints</name>
      <text>Setpoints</text>
      <width>270</width>
      <height>30</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblVolumeSet</name>
      <text>Volume:</text>
      <x>10</x>
      <y>66</y>
      <width>60</width>
      <vertical_alignment>2</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblRateSet</name>
      <text>Rate:</text>
      <x>10</x>
      <y>96</y>
      <width>60</width>
      <vertical_alignment>2</vertical_alignment>
    </widget>
    <widget type="textentry" version="3.0.0">
      <name>txtRate</name>
      <pv_name>$(P)$(R)SET_RATE</pv_name>
      <x>75</x>
      <y>96</y>
      <width>70</width>
      <height>25</height>
      <format>1</format>
      <precision>3</precision>
      <rules>
        <rule name="Alarmed" prop_id="background_color" out_exp="false">
          <exp bool_exp="pv0==3">
            <value>
              <color red="255" green="102" blue="102">
              </color>
            </value>
          </exp>
          <pv_name>$(P)$(R)SET_RATE.HHSV</pv_name>
        </rule>
      </rules>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="textentry" version="3.0.0">
      <name>txtVolume</name>
      <pv_name>$(P)$(R)SET_VOLUME</pv_name>
      <x>75</x>
      <y>66</y>
      <width>70</width>
      <height>25</height>
      <format>1</format>
      <precision>3</precision>
      <rules>
        <rule name="Alarmed" prop_id="background_color" out_exp="false">
          <exp bool_exp="pv0==3">
            <value>
              <color red="255" green="102" blue="102">
              </color>
            </value>
          </exp>
          <pv_name>$(P)$(R)SET_VOLUME.HHSV</pv_name>
        </rule>
      </rules>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="combo" version="2.0.0">
      <name>cboxVolumeUnitSet</name>
      <pv_name>$(P)$(R)SET_VOLUME_UNITS</pv_name>
      <x>153</x>
      <y>66</y>
      <width>107</width>
      <height>25</height>
      <actions>
        <action type="write_pv">
          <pv_name>$(pv_name)</pv_name>
          <value>$(item)</value>
          <description>Write PV</description>
        </action>
      </actions>
      <items>
        <item></item>
      </items>
    </widget>
    <widget type="combo" version="2.0.0">
      <name>cboxRateUnitSet</name>
      <pv_name>$(P)$(R)SET_RATE_UNITS</pv_name>
      <x>153</x>
      <y>96</y>
      <width>107</width>
      <height>25</height>
      <actions>
        <action type="write_pv">
          <pv_name>$(pv_name)</pv_name>
          <value>$(item)</value>
          <description>Write PV</description>
        </action>
      </actions>
      <items>
        <item></item>
      </items>
    </widget>
    <widget type="combo" version="2.0.0">
      <name>cboxDirectionSet</name>
      <pv_name>$(P)$(R)SET_DIRECTION</pv_name>
      <x>75</x>
      <y>126</y>
      <width>185</width>
      <height>25</height>
      <actions>
        <action type="write_pv">
          <pv_name>$(pv_name)</pv_name>
          <value>$(item)</value>
          <description>Write PV</description>
        </action>
      </actions>
      <items>
        <item></item>
      </items>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>txtMessage</name>
      <pv_name>$(P)$(R)MESSAGE_TEXT</pv_name>
      <x>75</x>
      <y>190</y>
      <width>185</width>
      <font>
        <font family="Liberation Sans" style="BOLD" size="12.0">
        </font>
      </font>
      <foreground_color>
        <color name="MAJOR" red="255" green="0" blue="0">
        </color>
      </foreground_color>
      <format>6</format>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblDirectionSet_1</name>
      <text>Error:</text>
      <x>10</x>
      <y>190</y>
      <width>60</width>
      <vertical_alignment>2</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblDiameterSet</name>
      <text>Inside diameter:</text>
      <x>10</x>
      <y>36</y>
      <vertical_alignment>2</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblDirectionSet</name>
      <text>Direction:</text>
      <x>10</x>
      <y>126</y>
      <width>60</width>
      <vertical_alignment>2</vertical_alignment>
    </widget>
    <widget type="textentry" version="3.0.0">
      <name>txtDiameterNum</name>
      <pv_name>$(P)$(R)SET_DIAMETER_NUM</pv_name>
      <x>153</x>
      <y>36</y>
      <width>45</width>
      <height>25</height>
      <format>1</format>
      <precision>0</precision>
      <rules>
        <rule name="Alarmed" prop_id="background_color" out_exp="false">
          <exp bool_exp="pv0==3">
            <value>
              <color red="255" green="102" blue="102">
              </color>
            </value>
          </exp>
          <pv_name>$(P)$(R)SET_DIAMETER.SEVR</pv_name>
        </rule>
      </rules>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="textentry" version="3.0.0">
      <name>txtDiameterDen</name>
      <pv_name>$(P)$(R)SET_DIAMETER_DEN</pv_name>
      <x>215</x>
      <y>36</y>
      <width>45</width>
      <height>25</height>
      <format>1</format>
      <precision>0</precision>
      <rules>
        <rule name="Alarmed" prop_id="background_color" out_exp="false">
          <exp bool_exp="pv0==3">
            <value>
              <color red="255" green="102" blue="102">
              </color>
            </value>
          </exp>
          <pv_name>$(P)$(R)SET_DIAMETER.SEVR</pv_name>
        </rule>
      </rules>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblDiameterSet_1</name>
      <text>/</text>
      <x>197</x>
      <y>38</y>
      <width>18</width>
      <font>
        <font family="Liberation Sans" style="BOLD" size="16.0">
        </font>
      </font>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>2</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblVolumeCalibSet</name>
      <text>Calibration volume:</text>
      <x>10</x>
      <y>156</y>
      <width>135</width>
      <vertical_alignment>2</vertical_alignment>
      <tooltip>Measured dispense volume to use when calibrating tubing diameter</tooltip>
    </widget>
    <widget type="textentry" version="3.0.0">
      <name>txtVolumeCalib</name>
      <pv_name>$(P)$(R)SET_CALIB</pv_name>
      <x>153</x>
      <y>156</y>
      <width>107</width>
      <height>25</height>
      <format>1</format>
      <precision>3</precision>
      <rules>
        <rule name="Alarmed" prop_id="background_color" out_exp="false">
          <exp bool_exp="pv0==3">
            <value>
              <color red="255" green="102" blue="102">
              </color>
            </value>
          </exp>
          <pv_name>$(P)$(R)SET_CALIB.HHSV</pv_name>
        </rule>
      </rules>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
  </widget>
  <widget type="group" version="2.0.0">
    <name>grpReadbacks</name>
    <x>298</x>
    <y>60</y>
    <width>220</width>
    <height>230</height>
    <style>3</style>
    <background_color>
      <color name="BACKGROUND" red="220" green="225" blue="221">
      </color>
    </background_color>
    <transparent>true</transparent>
    <widget type="label" version="2.0.0">
      <name>grpTitleReadbacks</name>
      <text>Readbacks</text>
      <width>220</width>
      <height>30</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblRateGet</name>
      <text>Rate:</text>
      <x>10</x>
      <y>97</y>
      <width>65</width>
      <vertical_alignment>2</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblDirectionGet</name>
      <text>Direction:</text>
      <x>10</x>
      <y>126</y>
      <width>65</width>
      <vertical_alignment>2</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblDiameterGet</name>
      <text>Diameter:</text>
      <x>10</x>
      <y>36</y>
      <width>65</width>
      <vertical_alignment>2</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblVolumeGet</name>
      <text>Volume:</text>
      <x>10</x>
      <y>66</y>
      <width>65</width>
      <vertical_alignment>2</vertical_alignment>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>txtVolumeValRead</name>
      <pv_name>$(P)$(R)VOLUME</pv_name>
      <x>77</x>
      <y>66</y>
      <width>60</width>
      <format>1</format>
      <precision>3</precision>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>txtRateValRead</name>
      <pv_name>$(P)$(R)RATE</pv_name>
      <x>77</x>
      <y>97</y>
      <width>60</width>
      <format>1</format>
      <precision>3</precision>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>txtDirectionValRead</name>
      <pv_name>$(P)$(R)DIRECTION</pv_name>
      <x>77</x>
      <y>126</y>
      <width>130</width>
      <format>6</format>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>txtDiameterValRead</name>
      <pv_name>$(P)$(R)DIAMETER</pv_name>
      <x>77</x>
      <y>36</y>
      <width>70</width>
      <format>1</format>
      <precision>3</precision>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>txtRateUnitRead</name>
      <pv_name>$(P)$(R)VOLUME_UNITS</pv_name>
      <x>137</x>
      <y>66</y>
      <width>70</width>
      <format>6</format>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>txtRateUnitRead</name>
      <pv_name>$(P)$(R)RATE_UNITS</pv_name>
      <x>137</x>
      <y>97</y>
      <width>70</width>
      <format>6</format>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblStatus</name>
      <text>Status:</text>
      <x>10</x>
      <y>190</y>
      <width>65</width>
      <vertical_alignment>2</vertical_alignment>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>txtStatus</name>
      <pv_name>$(P)$(R)STATUS_TEXT</pv_name>
      <x>77</x>
      <y>190</y>
      <width>130</width>
      <font>
        <font family="Liberation Sans" style="BOLD" size="12.0">
        </font>
      </font>
      <foreground_color>
        <color red="179" green="77" blue="26">
        </color>
      </foreground_color>
      <format>6</format>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblDiameterUnit</name>
      <text>inch</text>
      <x>147</x>
      <y>36</y>
      <width>60</width>
      <background_color>
        <color name="Read_Background" red="240" green="240" blue="240">
        </color>
      </background_color>
      <transparent>false</transparent>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
  </widget>
</display>
